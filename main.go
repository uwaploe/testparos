// Test a Paroscientific pressure sensor connected to a serial port
package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/mfkenney/paros"
	"bitbucket.org/uwaploe/testparos/internal/rdwriter"
)

const Usage = `Usage: testparos [options] serialdev

Test a Paroscientific pressure sensor connected to serialdev. The
command-line options are used to set the test duration and
sampling interval. The sample values are written to standard
output in CSV format (time, pressure, temperature) and optionally
published to a Redis pub-sub channel.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	testInterval time.Duration = time.Second * 5
	testDuration time.Duration = time.Second * 60
	intTime      time.Duration = time.Millisecond * 3500
	devBaud      int           = 9600
	outFile      string
	rdAddr       string = "localhost:6379"
	rdChan       string
)

const TimeFormat = "2006-01-02T15:04:05.000Z07:00"

func sampleSensor(ctx context.Context, dev *paros.Device, wtr io.Writer,
	interval, duration time.Duration) error {

	if interval > duration {
		return fmt.Errorf("Interval %q is greater than duration %q",
			interval, duration)
	}

	ticker := time.NewTicker(interval)
	defer ticker.Stop()
	timeout := time.NewTicker(duration)
	defer timeout.Stop()

	t := time.Now()
loop:
	for {
		m, err := dev.Sample()
		if err != nil {
			return err
		}
		fmt.Fprintf(wtr, "%s,%.3f,%.3f\n", t.Format(TimeFormat), m.Pr, m.Temp)
		select {
		case <-ctx.Done():
			return ctx.Err()
		case t = <-ticker.C:
		case <-timeout.C:
			break loop
		}
	}

	return nil
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.IntVar(&devBaud, "baud",
		lookupEnvOrInt("PAROS_BAUD", devBaud),
		"sensor baud rate")
	flag.DurationVar(&testInterval, "interval",
		lookupEnvOrDuration("PAROS_INTERVAL", testInterval),
		"test sample interval")
	flag.DurationVar(&testDuration, "duration",
		lookupEnvOrDuration("PAROS_DURATION", testDuration),
		"test duration")
	flag.DurationVar(&intTime, "int-time",
		lookupEnvOrDuration("PAROS_INT_TIME", intTime),
		"sensor integration time")
	flag.StringVar(&rdAddr, "rd-addr",
		lookupEnvOrString("REDIS_ADDR", rdAddr),
		"Redis server HOST:PORT")
	flag.StringVar(&rdChan, "rd-chan",
		lookupEnvOrString("REDIS_CHANNEL", rdChan),
		"Redis channel to publish data")

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		p   Port
		err error
	)

	if strings.Contains(args[0], ":") {
		p, err = NetworkPort(args[0], time.Second*5)
	} else {
		p, err = SerialPort(args[0], 9600, time.Second*5)
	}

	if err != nil {
		log.Fatalf("Cannot access device at %q: %v", args[0], err)
	}

	dev := paros.NewDevice(p, 1)
	info, err := dev.GetInfo()
	if err != nil {
		log.Fatalf("Device communication error: %v", err)
	}

	json.NewEncoder(os.Stderr).Encode(info)

	dev.SetIntTime(paros.Pressure, intTime)
	dev.SetIntTime(paros.Temperature, intTime)
	// Enable simultaneous integration
	dev.SendCmd("EW", "OI=0")

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Cancel the context on a signal interrupt
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	wtr := io.Writer(os.Stdout)
	if rdChan != "" {
		w := rdwriter.New(rdAddr, rdChan)
		if w != nil {
			wtr = io.MultiWriter(w, os.Stdout)
		}
	}

	sampleSensor(ctx, dev, wtr, testInterval, testDuration)
}
