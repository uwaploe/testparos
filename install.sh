#!/usr/bin/env bash

PROG="testparos"
BINDIR="$HOME/bin"
SVCDIR="$HOME/.config/systemd/user"

mkdir -p "$BINDIR"
cp -v $PROG "$BINDIR"
mkdir -p "$SVCDIR"
cp -v systemd/* "$SVCDIR"

systemctl --user stop 'testparos@*.service'
systemctl --user stop 'testparos@*.timer'
systemctl --user daemon-reload
