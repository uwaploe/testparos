// Package rdwriter provides an io.Writer interface to a Redis
// pub-sub channel
package rdwriter

import "github.com/gomodule/redigo/redis"

type Writer struct {
	conn    redis.Conn
	channel string
}

func New(addr string, channel string) *Writer {
	var err error
	w := Writer{channel: channel}
	w.conn, err = redis.Dial("tcp", addr)
	if err != nil {
		return nil
	}
	return &w
}

func (w *Writer) Write(p []byte) (int, error) {
	_, err := w.conn.Do("PUBLISH", w.channel, p)
	if err != nil {
		return 0, err
	}
	return len(p), nil
}
