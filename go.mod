module bitbucket.org/uwaploe/testparos

go 1.13

require (
	bitbucket.org/mfkenney/paros v0.2.2
	github.com/garyburd/redigo v1.6.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)
